$HistoryPath = "$HOME\WindowsPowerShell\History"

Function Export-History
{
    [CmdletBinding()]
    Param
    (
        [Parameter(
            mandatory=$false,
            Position=0,
            ValueFromPipeline=$True)]
        $CommandInfo,
        [Parameter(
            Mandatory=$false,
            Position=1,
            HelpMessage="Type Path for history file")]
        $Path = $HistoryPath,
        [Parameter(
            Mandatory=$false,
            Position=2,
            HelpMessage="Type Name for history file")]
        $Name = "History"
    )

    $Name = "$Name-" + (Get-Date -Format 'yyyy-MM-dd-HHmmss') + '.xml'

    if (-NOT (Test-Path "$Path"))
    {
        $null = New-Item -Path $Path -ItemType Directory
    }

    if(!$commandInfo)
    {
        Get-History | Export-Clixml -Path "$Path\$Name"
    }
    Else
    { 
        $input | Export-Clixml -Path "$Path\$Name" 
    }
}

function Import-History
{
    [CmdletBinding()]
    Param
    (
        [Parameter(
            Mandatory=$false,
            Position=0,
            HelpMessage="Type Path for history file")]
        $Path = $HistoryPath,
        [Parameter(
            Mandatory=$false,
            Position=1)]
        $Name = "History",
        [Parameter(
            Mandatory=$false,
            Position=2)]
        $Last=1
    )
    $File = Get-ChildItem -Path $Path -Filter "$Name*.xml" | sort LastWriteTime | select -Last $Last | select -First 1

    Add-History -InputObject (Import-Clixml "$Path/$File")
} 

function Compress-History 
{
    $new = Get-History | Group CommandLine | Foreach {$_.Group[0]}
    Clear-History 
    Add-History $new
}

Function Save-History
{
    $scriptBlock = [Scriptblock]::Create("Export-History") 
    Register-EngineEvent -SourceIdentifier powershell.exiting  -SupportEvent -Action $scriptBlock
}